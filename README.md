# VF / UI Gulp Watch #

Builds SCSS for **one** VF brand on-the-fly for you to host locally.

## Where to put it
This folder (containing the gulpfile.js) must be a sibling of the **ui** directory to grab the SCSS from the correct location.
```
{your computer}
    |-- vf-ui-gulp-watch 
        |-- gulpfile.js
        |-- package.json
    |-- ui 
        |-- src 
            |-- {brand directories} 
        |-- build
```
## Install Gulp & assorted dependencies
`npm install`

## Build SCSS
`gulp --brand="napapijri-eu"` 

## Watch SCSS
`gulp watch --brand="napapijri-eu"` 