const gulp = require('gulp'),
  yargs = require('yargs').argv,
  plumber = require('gulp-plumber'),
  sass = require('gulp-sass'),
  watch = require('gulp-watch'),
  path = require('path'),
  notify = require('gulp-notify');

// PATHS
const UI_SRC = '../ui/src',
  UI_BUILD = '../ui/build';


// Current Brand
const brand = yargs.brand,
  BRAND_DIR = UI_SRC + '/' + brand;


// SASS
const BRAND_SASS = BRAND_DIR + '/sass/**/*.scss',
  BRAND_SASS_TO_COMPILE = BRAND_DIR + '/sass/**/!(_)*.scss',
  SHARED_SASS = UI_SRC + '/_shared/sass',
  SASS_DEPENDENCIES = [
    SHARED_SASS,
    'node_modules/compass-mixins/lib'
  ],
  CURRENT_SASS = [
    BRAND_SASS,
    SHARED_SASS + '/**/*.scss'
  ];

// Sass task
gulp.task('sass', function() {

  return gulp.src(BRAND_SASS_TO_COMPILE)
    .pipe(plumber(plumberErrorHandler))
    .pipe(sass({
      includePaths: SASS_DEPENDENCIES,
      sourceComments: true,
      sourceMapEmbed: true,
      importer: function(url, prev, done) {
        // Custom importer to ignore current file and use shared sass instead for @imports
        let updatedUrl = url;
        let currentPath = prev.indexOf('/sass/') > -1 ? prev.split('/sass/')[1].replace(/(.*)_(.*)\.scss/g, '$1$2') : prev;

        if (updatedUrl == currentPath) { // Also don't change ones already explicitly set to .css
          updatedUrl = SHARED_SASS + '/' + url;
        }
        done({
          file: updatedUrl
        });
      }
    }).on('error', sass.logError))
    .pipe(gulp.dest(UI_BUILD + '/' + brand + '/css'));
});

var notifyInfo = {
  title: 'Gulp',
  icon: path.join(__dirname, 'gulp.png')
};

//error notification settings for plumber
var plumberErrorHandler = {
  errorHandler: notify.onError({
    title: notifyInfo.title,
    icon: notifyInfo.icon,
    message: 'Error: <%= error.message %>'
  })
};

// TASKS
gulp.task('default', ['sass']);

gulp.task('watch', function() {
  gulp.watch(CURRENT_SASS, ['sass']);
});